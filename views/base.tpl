<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .btn,
        .list-group-item,
        .card {
            border-radius: 0 !important
        }

        #headers {
            display: none;
            max-height: 200px;
            background-color: #eee;
        }
        .text-content {
            color: #333;
        }
    </style>
    <title>{{title or 'TempMBox'}}</title>
</head>
<body>
    {{!base}}
</body>
</html>