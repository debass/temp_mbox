# _*_ encoding: utf-8
import time

from fabric.api import cd, prefix, settings, env, run, sudo, execute
from fabric.contrib.files import exists

# import logging ; logging.basicConfig(level=logging.DEBUG)

env.hosts = ['188.166.110.165']
env.user = 'refresh'

BITBUCKET_SSH_KEY = """bitbucket.org,104.192.143.1 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw=="""

ROOT_PATH = '/var/opt/temp_mbox'
REPO_PATH = '{}/temp_mbox'.format(ROOT_PATH)
VENV_PATH = '{}/.venv'.format(REPO_PATH)

ACTIVATE_VENV = 'source {venv}/bin/activate'.format(venv=VENV_PATH)



def sup_stop (arg):
    return "supervisorctl stop {} | grep -v 'not running'".format(arg)

def sup_start(arg):
    return """supervisorctl start {} 2>&1 | grep -v -e 'already started' -e ': started' """.format(arg)

def install_git ():
    with settings(warn_only = True):
        if not run("dpkg -s git | grep -q 'ok installed'").succeeded:
            sudo ("apt-get -y install git")

def add_bitbucket_ssh_key ():
    with settings(warn_only = True):
        run("touch  ~/.ssh/known_hosts")
        if not run("cat ~/.ssh/known_hosts | grep -q 'bitbucket.org'").succeeded:
            run(""" echo -e '{}' > ~/.ssh/known_hosts """.format(BITBUCKET_SSH_KEY))


def update_repo (branch):
    with settings(warn_only = True):
        if not exists(ROOT_PATH):
            sudo('mkdir {root}'.format(root=ROOT_PATH))
            sudo('chown -R refresh:refresh {root}'.format(root=ROOT_PATH))
        if not exists(REPO_PATH):
            with cd(ROOT_PATH):
                run('git clone git@bitbucket.org:debass/temp_mbox.git')
                run('git checkout --quiet --force {branch}'.format(branch=branch))
        else:
            with cd(REPO_PATH):
                run('git fetch --quiet')
                run('git checkout --quiet --force {branch}'.format(branch=branch))
                run('git pull origin {branch}'.format(branch=branch))

def install_and_config_nginx():
    with settings(warn_only = True):
        if not run("dpkg -s nginx | grep -q 'ok installed'").succeeded:
            sudo("apt-get -y -qq install nginx")

        sudo('cp -f {repo}/deploy/stuff/nginx/temp_mbox.conf /etc/nginx/sites-enabled/'.format(repo=REPO_PATH))

        sudo("service nginx restart")

def install_supervisord():
    with settings(warn_only = True):
        if not sudo('supervisorctl status').succeeded:
            sudo("apt-get -y -qq install supervisor")


def install_pip():
    with settings(warn_only = True):
        if not run('pip list').succeeded:
            sudo("apt-get -y -qq install python-pip python-dev build-essential")

def config_venv():
    with settings(warn_only = True):
        run("pip install virtualenv")

        if not exists(VENV_PATH):
            run('virtualenv {}'.format(VENV_PATH))

def stop_services():
    with settings(warn_only = True):
        sudo(sup_stop('mbox_webui mbox_smtp_server'))

def update_python_requirements():
    with cd(REPO_PATH), prefix(ACTIVATE_VENV), settings(user='refresh', warn_only = True):
        run('pip install -r requirements.txt')


def update_supervisor_config():
    with cd(REPO_PATH), settings(user='refresh', warn_only = True):
        sudo('cp -f {repo}/deploy/stuff/supervisor/temp_mbox.conf /etc/supervisor/conf.d/'.format(repo=REPO_PATH))
        sudo('supervisorctl update')

def start_services():
    with settings(user='refresh', warn_only = True):
        sudo(sup_start('mbox_webui mbox_smtp_server'))


def _deploy (branch = 'master'):
    start_time = time.time()

    install_git()
    add_bitbucket_ssh_key()
    update_repo(branch)

    install_and_config_nginx()
    install_supervisord()
    install_pip()

    config_venv()

    stop_services()
    update_python_requirements()
    update_supervisor_config()
    start_services()

    print 'Took {} seconds'.format(time.time() - start_time)


def deploy(branch = 'master'):
    execute(_deploy, branch = branch)
