% rebase('base.tpl', title='Mailbox: ' + mailbox_name)
<div class="container">
    <h1 class="text-center mt-3">{{mailbox_name}}@{{config.DOMAIN}}</h1>
    <button type="button" class="btn btn-block btn-outline-primary" onclick="location.reload();">Refresh</button>
    %if mails:
    <div class="list-group mb-3 mt-3">
        %for m in mails:
            <a href="/m/{{mailbox_name}}/{{m['id']}}" class="list-group-item-action list-group-item flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">From: <span style="color: #333">{{m['from']}}</span></h5>
                    <small>{{m['ctime']}}</small>
                </div>
                <p class="mb-1"> Subj: {{m['subject']}}</p>
            </a>
        %end
    </div>
    %else:
        <h3 class="text-center mt-3">No mails</h3>
    %end
</div>