% rebase('base.tpl', title='Error ' + error)
<div class="container">
    <h1 class="mt-3 text-center"> Error {{error}}! {{description}} </h1>
</div>