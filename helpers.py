import email
import string
import random
import sqlite3
import email.utils
from email.header import decode_header

import config

def init_db ():
    conn = sqlite3.connect(config.DB_FILE, detect_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)

    # https://stackoverflow.com/questions/3425320/sqlite3-programmingerror-you-must-not-use-8-bit-bytestrings-unless-you-use-a-te
    conn.text_factory = str

    with conn:
        conn.execute('''
            CREATE TABLE IF NOT EXISTS mails (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                recipient TEXT NOT NULL,
                sender TEXT NOT NULL,
                subject TEXT NOT NULL DEFAULT '',
                has_attachments BOOL NOT NULL DEFAULT FALSE,
                data TEXT NOT NULL,
                ctime DATETIME NOT NULL DEFAULT (datetime('now'))
            );
        ''')
    return conn


FORMAT_DT_TEMPLATE = '%H:%M:%S %d/%m/%Y'

def format_dt (dt):
    return dt.strftime(FORMAT_DT_TEMPLATE)


#https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
def sizeof_fmt(num, suffix='B'):
    for unit in ['','K','M','G']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


def generate_email_address(length = 10):
    return ''.join([random.choice(string.letters) for x in range(length + 1)])


def _decode_text (s):
    text, encoding = decode_header(s)[0]
    if encoding:
        text = text.decode(encoding)

    return text


def parse_attachment (message_part):
    attachment = {}

    file_data = message_part.get_payload(decode = True)
    attachment['content_type'] = message_part.get_content_type()
    attachment['size'] = len(file_data)
    attachment['human_size'] = sizeof_fmt(len(file_data))

    content_disposition = message_part.get("Content-Disposition", None)
    dispositions = content_disposition.strip().split(";")

    for param in dispositions[1:]:
        name, value = param.split("=", 1)
        name = name.strip().lower()
        value = value.strip().replace('"', '')

        if name == "filename":
            attachment['name'] = _decode_text(value)
        elif name == "create-date":
            attachment['create_date'] = email.utils.parsedate(value)
        elif name == "modification-date":
            attachment['mod_date'] = email.utils.parsedate(value)
        elif name == "read-date":
            attachment['read_date'] = email.utils.parsedate(value)

    return attachment

def parse_headers (message):
    normalize_address = ['From', 'To']

    headers = {}
    headers['raw'] = message.items()

    # extract and normalize addreses
    for n in normalize_address:
        result = []
        for name, email_address in email.utils.getaddresses(message.get_all(n, [])):

            if name:
                name = _decode_text(name)

            result.append([name, email_address])
        headers[n] = result

    # normalize subject
    subject = message.get('Subject', '')
    if subject:
        subject = _decode_text(subject)

    headers['Subject'] = subject

    other_headers = set(message.keys()) - set(normalize_address + ['Subject'])

    for k in other_headers:
        headers[k] = message.get(k)

    return headers


def parse_mail (m):
    message = email.message_from_string(m)

    body = {}
    attachments = []
    headers = parse_headers(message)

    if message.is_multipart():
        for part in message.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))

            if 'attachment' in cdispo:
                attachments.append(parse_attachment(part))

            elif ctype in ['text/html' or 'text/plain']:
                body['content_type'] = ctype
                body['content'] = unicode(part.get_payload(decode = True), part.get_content_charset() or 'utf-8', 'replace').encode('utf8','replace')

    else:
        body['content_type'] = message.get_content_type()
        body['content'] = unicode(message.get_payload(decode = True), message.get_content_charset() or 'utf-8', 'replace').encode('utf8','replace')

    return headers, body, attachments
