from helpers import init_db
import config

def main ():
    conn = init_db()

    with conn:
        conn.execute('''DELETE FROM mails WHERE ctime < datetime('now', '-{} minutes')'''.format(config.HOUSEKEEPER_MINUTES_INTERVAL))


if __name__ == '__main__':
    main()