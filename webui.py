import bottle
from bottle import route, run, redirect, view, error, abort

from helpers import init_db, parse_mail, format_dt, generate_email_address
import config

db_conn = init_db()

@route('/')
def root():
    redirect("/m/{}".format(generate_email_address(10)))


@route('/m/<mailbox_name>')
@view('mailbox_view')
def mailbox_view(mailbox_name):
    cur = db_conn.cursor()

    cur.execute("""
        SELECT
            id,
            recipient,
            sender,
            subject,
            has_attachments,
            ctime AS "[timestamp]"
        FROM mails
        WHERE recipient LIKE ?
        ORDER BY ctime DESC
    """, ('{}@%'.format(mailbox_name),))

    mails = cur.fetchall()

    cur.close()

    return {
        'config': config,
        'mailbox_name': mailbox_name,
        'mails': [
            {
                'id': m[0],
                'to': m[1],
                'from': m[2],
                'subject': m[3],
                'has_attachments': m[4],
                'ctime': format_dt(m[5])
            } for m in mails]
    }


@route('/m/<mailbox_name>/<mail_id>')
@view('mail_view')
def mail_view(mailbox_name, mail_id):
    cur = db_conn.cursor()

    cur.execute("""SELECT id, data, ctime FROM mails WHERE recipient LIKE ? AND id = ? """, ('{}@%'.format(mailbox_name), mail_id))
    mail = cur.fetchone()
    cur.close()

    if not mail:
        return abort(404)

    header, body, att = parse_mail(mail[1])
    # print header['raw']

    return {
        'config': config,
        'mailbox_name': mailbox_name,
        'header': header,
        'body': body,
        'attachments': att,
        'ctime': mail[2]
    }


@route('/admin')
@view('admin_view')
def admin_view ():
    cur = db_conn.cursor()
    cur.execute("""SELECT recipient, COUNT(id), MAX(ctime) AS "[timestamp]" FROM mails ORDER BY id DESC""")
    mailboxes = cur.fetchall()

    return {
        'config': config,
        'mailboxes': mailboxes
    }

@error(404)
@view('error_page')
def not_found (error):
    return {'error': '404', 'description': 'Page not found =('}


if __name__ == '__main__':
    bottle.debug(config.DEBUG)
    run(host='localhost', port=config.WEBUI_PORT, reloader = config.DEBUG, server='gunicorn', workers=4)
