# Temp MailBOX

Простая реализация сервиса одноразовой/анонимной/временной почты.


## Как это работает

Пользователь заходит на страницу сервиса, в этот момент генеируется новый email адрес и происходит редирект на страницу почтового ящика. При получении этим пользователем писем они будут отображаться в интерфейсе. Все предельно просто =)


```deploy/``` - деплой этого сервиса с помощью fabric

```views/``` - шаблоны для webui, используется встроенный в bottle движек.

```smtp_server.py``` - SMTP сервер, который принимает все входящие сообщения и кладывает их в SQLite базу.

```webui.py``` - простая реализация WEB UI, на базе bottle.py, для получения временного почтового ящика и просмотра сообщений

```housekeeper.py``` - скрипт для отчистки базы от старых сообщений



Для тестирования достаточно запустить локально smtp_server.py и webui.py и отсылать сообщения, например так:

~~~~
#encoding: utf-8

import smtplib
import email.utils
from email.mime.text import MIMEText

# Create the message
msg = MIMEText('Some text')
msg['To'] = email.utils.formataddr(('Recipient', 'wITBagBMppj@telejournal.xyz'))
msg['From'] = email.utils.formataddr(('Author', 'author@example.com'))
msg['Subject'] = 'Simple test message'

server = smtplib.SMTP('127.0.0.1', 1025)
server.set_debuglevel(True) # show communication with the server
try:
    server.sendmail('author@example.com', ['wITBagBMppj@telejournal.xyz', 'recipient2@exmaple.com'], msg.as_string())
finally:
    server.quit()
~~~~

Если планиутся использовать за пределами локального деплоймента, то необходимо настроить MX записи.
