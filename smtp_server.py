import smtpd
import asyncore
import sys
import logging

from helpers import init_db, parse_mail
import config

logging.basicConfig(level = logging.DEBUG, format = '%(asctime)s [%(levelname)s]: %(message)s')

class TempMboxServer (smtpd.SMTPServer):

    def __init__ (self, db_conn, localaddr):
        self.db_conn = db_conn

        smtpd.SMTPServer.__init__(self, localaddr, None)

    @staticmethod
    def check_domain (email):
        return email.split('@', 1)[1] == config.DOMAIN

    def process_message(self, peer, mailfrom, rcpttos, data):
        headers, body, att = parse_mail(data)
        logging.debug('New email! From: %s, To: %s', mailfrom, '; '.join(rcpttos))

        for r in rcpttos:

            if not self.check_domain(r):
                logging.error('Reject mail to %s. Invalid domain!', r)
                continue

            with self.db_conn:
                self.db_conn.execute("""
                    INSERT INTO mails (recipient, sender, subject, has_attachments, data) VALUES (?, ?, ?, ?, ?)
                """, (r, mailfrom, headers.get('Subject', ''), len(att) > 0, data))


if __name__ == '__main__':

    db_conn = init_db()

    server = TempMboxServer(db_conn, ('0.0.0.0', config.SMTP_PORT))
    logging.info('Server started!')

    try:
        asyncore.loop()
    except KeyboardInterrupt:
        logging.info('Exit...')
        sys.exit()