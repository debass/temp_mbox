% rebase('base.tpl', title='Mailbox: ' + mailbox_name)
<script type="text/javascript">
    function toggle_headers (el) {
        current_state = el.attributes['data-state'].nodeValue
        target_block = el.attributes['data-target_block'].nodeValue
        target_el = document.querySelector(target_block)
        target_el.style.display = current_state == 'close' ? 'block' : 'none'
        el.setAttribute('data-state', current_state == 'close' ? 'show' : 'close')
    }
</script>
<div class="container-fluid">
    <h1 class="text-center mt-3">{{mailbox_name}}@{{config.DOMAIN}}</h1>
        <div class="card mt-3 mb-3" style="width: 100%">
            <div class="card-header">
                <a href="/m/{{mailbox_name}}" class="btn btn-primary">Back</a>
            </div>
            <div class="card-body">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="card-title">{{header['Subject']}}</h5>
                    <small>{{ctime}}</small>
                </div>
                
                <h6 class="card-subtitle mb-2 text-muted">
                    {{header['From'][0][0]}} <a href="mailto:{{header['From'][0][1]}}">{{header['From'][0][1]}}</a>
                </h6>

                <button type="button" class="btn btn-outline-info btn-sm" data-target_block="#headers" data-state="close" onclick="toggle_headers(this);">Headers</button>

                <pre id="headers" class="mt-2 p-2 mb-0">{{'\n'.join(['{}:{}'.format(k,v) for k,v in header['raw']])}}</pre>

                <p class="card-text mt-3" style="border-top: 1px solid #eee">
                    <p>
                    %if body['content_type'] == 'text/plain':
                        <code class="text-content">
                            {{body['content']}}
                        </code>
                    %else:
                        <div style="overflow: scroll">
                            {{!body['content']}}
                        </div>
                    %end
                    </p>
                </p>

            </div>
            % if attachments:
                <div class="card-footer">
                %for a in attachments:
                    {{a['name']}} | {{a['human_size']}} bytes <br>
                %end
                </div>
            %end
        </div>
    </div>
</div>
