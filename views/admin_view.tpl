% rebase('base.tpl', title='Admin page')
<div class="container">
    %if mailboxes:
        <table class="table mt-3">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Mailbox</th>
                    <th scope="col">Messages</th>
                    <th scope="col">Last message at</th>
                </tr>
            </thead>
            <tbody>
                %for i, m in enumerate(mailboxes):
                    <tr>
                        <td>{{i + 1}}</td>
                        <td><a href="/m/{{m[0].split('@')[0]}}">{{m[0]}}</a></td>
                        <td>{{m[1]}}</td>
                        <td>{{m[2]}}</td>
                    </tr>
                %end
            </tbody>
        </table>
    %else:
        <h1> No mailboxes </h1>
    %end
</div>